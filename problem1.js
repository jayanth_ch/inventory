/* ==== Problem #1 ====
 The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer 
 find out which car has an id of 33 by calling a function that will return the data for that car. 
 Then log the car's year, make, and model in the console log in the format of: 
"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
*/
function p1 (inventory,id){
    if (inventory == undefined || inventory.length==0 || id == undefined)
    {
return 1;
    }
    
for (i=0;i<inventory.length;i++){
    let pro = inventory[i];
    if (pro.id == id){
    
        //('Car'+' '+pro.id+' '+'is a'+' '+pro.car_year+' '+pro.car_make+' '+pro.car_model);
        return pro;
    
        }
}
    return 1;
}
module.exports=p1;
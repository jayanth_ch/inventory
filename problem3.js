// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. 
//Execute a function to Sort all the car model names into 
//alphabetical order and log the results in the console as it was returned.
function carlist(inventory){
    var newList = [];
    
    for (i=0;i<inventory.length;i++){
        newList.push(inventory[i].car_model);
    }
return newList.sort();
} 
module.exports=carlist;